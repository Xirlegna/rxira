<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    public function run()
    {
        collect([
            ['name' => 'Sadmin User', 'email' => 'sadmin.user@mail.com'],
            ['name' => 'Admin User', 'email' => 'admin.user@mail.com'],
            ['name' => 'General User', 'email' => 'general.user@mail.com'],
            ['name' => 'Another General User', 'email' => 'another.general.user@mail.com']
        ])->each(function ($data) {
            $user = new User;
            $user->name = $data['name'];
            $user->email = $data['email'];
            $user->password = bcrypt('Wasd1234');
            $user->save();
        });
    }
}
