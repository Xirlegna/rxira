<?php

namespace Database\Seeders;

use App\Models\Role;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    public function run()
    {
        collect([
            Role::GENERAL,
            Role::ADMIN,
            Role::SUEPR_ADMIN
        ])->each(function ($data) {
            $role = new Role;
            $role->name = $data;
            $role->save();
        });
    }
}
