<?php

namespace Database\Seeders;

use App\Models\Product;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class ProductSeeder extends Seeder
{
    public function run()
    {
        collect([
            [
                'name' => 'Product A',
                'slug' => Str::slug('Product A'),
                'price' => 1999,
                'description' => 'Product A desc'
            ],
            [
                'name' => 'Product B',
                'slug' => Str::slug('Product B'),
                'price' => 2499,
                'description' => 'Product B desc'
            ],
            [
                'name' => 'Product C',
                'slug' => Str::slug('Product C'),
                'price' => 2999,
                'description' => 'Product C desc'
            ],
            [
                'name' => 'Product D',
                'slug' => Str::slug('Product D'),
                'price' => 3499,
                'description' => 'Product D desc'
            ]
        ])->each(function ($data) {
            $product = new Product;
            $product->name = $data['name'];
            $product->slug = $data['slug'];
            $product->price = $data['price'];
            $product->description = $data['description'];
            $product->save();
        });
    }
}
