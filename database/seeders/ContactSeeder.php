<?php

namespace Database\Seeders;

use App\Models\Contact;
use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;

class ContactSeeder extends Seeder
{
    public function run()
    {
        collect([
            [
                'email' => 'sadmin.user@mail.com',
                'phone' => '+36301111111',
                'role' => Role::SUEPR_ADMIN
            ],
            [
                'email' => 'admin.user@mail.com',
                'phone' => '+36302222222',
                'role' => Role::ADMIN
            ],
            [
                'email' => 'general.user@mail.com',
                'phone' => '+36303333333',
                'role' => Role::GENERAL
            ],
            [
                'email' => 'another.general.user@mail.com',
                'phone' => '+36304444444',
                'role' => Role::GENERAL
            ]
        ])->each(function ($data) {
            $user = User::where('email', $data['email'])->first();
            $role = Role::where('name', $data['role'])->first();

            $contact = new Contact;
            $contact->phone = $data['phone'];
            $contact->role()->associate($role);

            $user->contact()->save($contact);

            $contact->save();
        });
    }
}
