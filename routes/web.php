<?php

use Illuminate\Support\Facades\Route;

Route::get('{any}', function () {
    return view('welcome');
})->where('any', '.*');

Route::get('/clear-cache-all', function () {
    Artisan::call('cache:clear');

    dd("Cache Clear All");
});
