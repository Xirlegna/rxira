<!DOCTYPE html>
<html lang="hu">
    <head>
        <meta charset="utf-8">
        <meta name="robots" content="noindex">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600" rel="stylesheet">
        <title>RXIRA</title>
        <script src="{{ asset('js/admin.js') }}" defer></script>
        <link href="{{ asset('/css/admin.css') }}" rel="stylesheet" />
    </head>
    <body>
        <div id="root"></div>
    </body>
</html>