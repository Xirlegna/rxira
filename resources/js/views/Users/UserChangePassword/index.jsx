import React from 'react';
import UserChangePasswordForm from './UserChangePasswordForm';

const UserChangePassword = () => {
    return (
        <>
            <div className="section-header">
                <div className="section-header__title">User Change Password</div>
            </div>
            <UserChangePasswordForm />
        </>
    );
};

export default UserChangePassword;