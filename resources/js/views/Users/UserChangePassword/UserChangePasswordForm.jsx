import React, { useState } from 'react';
import { Link, useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { Field, reduxForm } from 'redux-form';
import useAxios from '../../../axios';
import { Group, Row } from '../../../components/Form/Form';
import PasswordInput from '../../../components/Form/Input/PasswordInput';
import * as MS from '../../../components/MessageList/messageStyles';
import * as actions from '../../../store/actions/index';

const UserChangePasswordForm = ({ handleSubmit }) => {
    const auth = useSelector((state) => state.auth);
    const history = useHistory();
    const [error, setError] = useState({});

    const dispatch = useDispatch();
    const onShowSpinner = () => dispatch(actions.showSpinner());
    const onHideSpinner = () => dispatch(actions.hideSpinner());
    const onMessage = (style, message) => dispatch(actions.message(style, message));
    const axios = useAxios();

    const onSubmitForm = async (form) => {
        onShowSpinner();

        const path = `users/change-password/${auth.id}`;
        const body = { ...form };

        await axios.patch(path, body)
            .then(() => {
                onMessage(MS.SUCCESS, 'Sikeres jelszó változtatás');
                history.push("/");
            }).catch((errors) => {
                switch (errors.response.status) {
                    case 422:
                        setError(errors.response.data.errors);
                        break;
                    default:
                        onMessage(MS.ERROR, 'Ismeretlen hiba történt');
                }
            }).finally(() => {
                onHideSpinner();
            });
    };

    return (
        <form className="section" onSubmit={handleSubmit(onSubmitForm)}>
            <Row>
                <Group col="6">
                    <Field
                        label="Old password"
                        name="old_password"
                        error={error.old_password}
                        component={PasswordInput}
                    />
                </Group>
            </Row>
            <Row>
                <Group col="6">
                    <Field
                        label="New password"
                        name="password"
                        error={error.password}
                        component={PasswordInput}
                    />
                </Group>
                <Group col="6">
                    <Field
                        label="New password confirmation"
                        name="password_confirmation"
                        component={PasswordInput}
                    />
                </Group>
            </Row>
            <Row>
                <button type="submit" className="btn btn--purple">Elfogad</button>
                <Link to="/" className="btn btn--gray">Vissza</Link>
            </Row>
        </form>
    );
};

export default reduxForm({ form: 'user_change_password_form' })(UserChangePasswordForm);