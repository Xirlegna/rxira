import React from 'react';
import UserCreateForm from './UserCreateForm';

const UserCreate = () => {
    return (
        <>
            <div className="section-header">
                <div className="section-header__title">User Create</div>
            </div>
            <UserCreateForm initialValues={{ role: 'GENERAL' }} />
        </>
    );
};

export default UserCreate;