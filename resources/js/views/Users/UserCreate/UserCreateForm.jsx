import React, { useState } from 'react';
import { Link, useHistory } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { Field, reduxForm } from 'redux-form';
import useAxios from '../../../axios';
import { Group, Row } from '../../../components/Form/Form';
import PasswordInput from '../../../components/Form/Input/PasswordInput';
import * as MS from '../../../components/MessageList/messageStyles';
import * as actions from '../../../store/actions/index';

const UserCreateForm = ({ handleSubmit }) => {
    const history = useHistory();
    const [error, setError] = useState({});

    const dispatch = useDispatch();
    const onShowSpinner = () => dispatch(actions.showSpinner());
    const onHideSpinner = () => dispatch(actions.hideSpinner());
    const onMessage = (style, message) => dispatch(actions.message(style, message));
    const axios = useAxios();

    const onSubmitForm = async (form) => {
        onShowSpinner();
        const path = 'users';
        const body = { ...form };

        await axios.post(path, body)
            .then(() => {
                history.push('/users');
            }).catch((errors) => {
                switch (errors.response.status) {
                    case 422:
                        setError(errors.response.data.errors);
                        break;
                    default:
                        if (errors.response.data.message.includes('users_email_unique')) {
                            onMessage(MS.ERROR, 'A választott e-mail cím már foglalt');
                        } else {
                            onMessage(MS.ERROR, 'Ismeretlen hiba történt');
                        }
                }
            }).finally(() => {
                onHideSpinner();
            });
    };

    return (
        <form className="section" onSubmit={handleSubmit(onSubmitForm)}>
            <Row>
                <Group col="6">
                    <label htmlFor="name" className="section__label">Name</label>
                    <Field name="name" component="input" type="text" className="section__input" />
                    {error.name && <p className="section__error">{error.name}</p>}
                </Group>
                <Group col="6">
                    <label htmlFor="email" className="section__label">E-mail</label>
                    <Field name="email" component="input" type="text" className="section__input" />
                    {error.email && <p className="section__error">{error.email}</p>}
                </Group>
            </Row>
            <Row>
                <Group col="6">
                    <Field
                        label="Password"
                        name="password"
                        error={error.password}
                        component={PasswordInput}
                    />
                </Group>
                <Group col="6">
                    <label htmlFor="role" className="section__label">Role</label>
                    <Field name="role" component="select" className="section__input">
                        <option value="GENERAL">General</option>
                        <option value="ADMIN">Admin</option>
                        <option value="SUEPR_ADMIN">Super Admin</option>
                    </Field>
                    {error.role && <p className="section__error">{error.role}</p>}
                </Group>
            </Row>
            <Row>
                <button type="submit" className="btn btn--purple">Elfogad</button>
                <Link to="/users" className="btn btn--gray">Vissza</Link>
            </Row>
        </form>
    );
};

export default reduxForm({
    form: 'user_edit_form',
    enableReinitialize: true
})(UserCreateForm);