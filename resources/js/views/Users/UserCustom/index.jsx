import React, { useCallback, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import UserCustomForm from './UserCustomForm';
import useAxios from '../../../axios';
import * as MS from '../../../components/MessageList/messageStyles';
import * as actions from '../../../store/actions/index';

const UserCustom = () => {
    const auth = useSelector((state) => state.auth);
    const [params, setParams] = useState({});

    const dispatch = useDispatch();
    const onShowSpinner = () => dispatch(actions.showSpinner());
    const onHideSpinner = () => dispatch(actions.hideSpinner());
    const onMessage = (style, message) => dispatch(actions.message(style, message));
    const axios = useAxios();

    const load = useCallback(async () => {
        onShowSpinner();
        await axios.get(`users/${auth.id}`)
            .then((response) => {
                setParams({
                    name: response.data.data.name,
                    email: response.data.data.email,
                    role: response.data.data.role
                });
            })
            .catch(() => {
                onMessage(MS.ERROR, 'Ismeretlen hiba történt');
            }).finally(() => {
                onHideSpinner();
            });
    }, [auth.id]);

    useEffect(() => { load(); }, [load]);

    return (
        <>
            <div className="section-header">
                <div className="section-header__title">Profil szerkesztés</div>
            </div>
            <UserCustomForm initialValues={params} />
        </>
    );
};

export default UserCustom;