import React, { useState } from 'react';
import { useParams } from 'react-router';
import { Link, useHistory } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { Field, reduxForm } from 'redux-form';
import useAxios from '../../../axios';
import { Group, Row } from '../../../components/Form/Form';
import * as MS from '../../../components/MessageList/messageStyles';
import * as actions from '../../../store/actions/index';

const UserEditForm = ({ handleSubmit }) => {
    const history = useHistory();
    const { id } = useParams();
    const [error, setError] = useState({});

    const dispatch = useDispatch();
    const onShowSpinner = () => dispatch(actions.showSpinner());
    const onHideSpinner = () => dispatch(actions.hideSpinner());
    const onMessage = (style, message) => dispatch(actions.message(style, message));
    const axios = useAxios();

    const onSubmitForm = async (form) => {
        onShowSpinner();
        const path = `users/${id}`;
        const body = { ...form };

        await axios.patch(path, body)
            .then(() => {
                history.push("/users");
            }).catch((errors) => {
                switch (errors.response.status) {
                    case 422:
                        setError(errors.response.data.errors);
                        break;
                    default:
                        onMessage(MS.ERROR, 'Ismeretlen hiba történt');
                }
            }).finally(() => {
                onHideSpinner();
            });
    };

    return (
        <form className="section" onSubmit={handleSubmit(onSubmitForm)}>
            <Row>
                <Group col="6">
                    <label htmlFor="name" className="section__label">Név</label>
                    <Field name="name" component="input" type="text" className="section__input" />
                    {error.name && <p className="section__error">{error.name}</p>}
                </Group>
                <Group col="6">
                    <label htmlFor="email" className="section__label">E-mail</label>
                    <Field name="email" component="input" type="text" className="section__input" />
                    {error.email && <p className="section__error">{error.email}</p>}
                </Group>
            </Row>
            <Row>
                <Group col="6">
                    <label htmlFor="role" className="section__label">Szerep</label>
                    <Field name="role" component="select" className="section__input">
                        <option value="GENERAL">Normál</option>
                        <option value="ADMIN">Admin</option>
                        <option value="SUEPR_ADMIN">Szuper Admin</option>
                    </Field>
                    {error.role && <p className="section__error">{error.role}</p>}
                </Group>
            </Row>
            <Row>
                <button type="submit" className="btn btn--purple">Elfogad</button>
                <Link to="/users" className="btn btn--gray">Vissza</Link>
            </Row>
        </form>
    );
};

export default reduxForm({
    form: 'user_edit_form',
    enableReinitialize: true
})(UserEditForm);