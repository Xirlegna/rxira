import React, { useCallback, useEffect, useState } from 'react';
import { useParams } from 'react-router';
import { useDispatch } from 'react-redux';
import UserEditForm from './UserEditForm';
import useAxios from '../../../axios';
import * as MS from '../../../components/MessageList/messageStyles';
import * as actions from '../../../store/actions/index';

const UserEdit = () => {
    const { id } = useParams();
    const [params, setParams] = useState({});

    const dispatch = useDispatch();
    const onShowSpinner = () => dispatch(actions.showSpinner());
    const onHideSpinner = () => dispatch(actions.hideSpinner());
    const onMessage = (style, message) => dispatch(actions.message(style, message));
    const axios = useAxios();

    const load = useCallback(async () => {
        onShowSpinner();
        await axios.get(`users/${id}`)
            .then((response) => {
                setParams({
                    name: response.data.data.name,
                    email: response.data.data.email,
                    role: response.data.data.role
                });
            })
            .catch(() => {
                onMessage(MS.ERROR, 'Ismeretlen hiba történt');
            }).finally(() => {
                onHideSpinner();
            });
    }, [id]);

    useEffect(() => { load(); }, [load]);

    return (
        <>
            <div className="section-header">
                <div className="section-header__title">
                    {params?.name && (`${params?.name} szerkesztése`)}
                </div>
            </div>
            <UserEditForm initialValues={params} />
        </>
    );
};

export default UserEdit;