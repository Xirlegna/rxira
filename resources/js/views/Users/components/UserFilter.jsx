import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import SortAsc from '../../../components/Icons/SortAsc';
import SortDesc from '../../../components/Icons/SortDesc';
import parseUrl from '../../../helpers/parseUrlHelper';

const UserFilter = () => {
    const history = useHistory();

    const [querys, setQuerys] = useState([]);
    const [order, setOrder] = useState('asc');
    const [orderBy, setOrderBy] = useState('date');

    useEffect(() => {
        const urlParams = parseUrl();

        setQuerys(urlParams.filter((param) => !['order', 'order-by'].includes(param.key)));
        setOrder(urlParams.find((param) => param.key === 'order').value);
        setOrderBy(urlParams.find((param) => param.key === 'order-by').value);
    }, []);

    useEffect(() => {
        if (querys.length <= 0) {
            return;
        }

        const urlParams = parseUrl();
        const currentQuerys = urlParams.filter((param) => !['order', 'order-by'].includes(param.key));

        let url = `${window.location.pathname}?`;
        url += `${currentQuerys.map((query) => `${query.key}=${query.value}`).join('&')}&`;
        url += `order=${order}&order-by=${orderBy}`;

        history.push(url);
    }, [querys, order, orderBy]);

    const onSetOrder = () => {
        order === 'desc' ? setOrder('asc') : setOrder('desc');
    }

    return (
        <div className="filter">
            <select
                className="filter__select"
                value={orderBy}
                onChange={(e) => setOrderBy(e.target.value)}
            >
                <option value="name">Név</option>
                <option value="email">E-mail</option>
                <option value="date">Felvétel dátuma</option>
            </select>
            <div className="filter__btn" onClick={() => onSetOrder()} >
                {
                    order === 'asc'
                        ? <SortAsc className="filter__btn-icon" />
                        : <SortDesc className="filter__btn-icon" />
                }
            </div>
        </div>
    );
};

export default UserFilter;