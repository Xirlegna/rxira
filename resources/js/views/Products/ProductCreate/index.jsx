import React from 'react';
import ProductCreateForm from './ProductCreateForm';

const ProductCreate = () => {
    return (
        <>
            <div className="section-header">
                <div className="section-header__title">Product Create</div>
            </div>
            <ProductCreateForm />
        </>
    );
};

export default ProductCreate;