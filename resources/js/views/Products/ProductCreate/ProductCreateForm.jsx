import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { reduxForm } from 'redux-form';
import ProductForm from '../components/ProductForm';
import useAxios from '../../../axios';
import * as MS from '../../../components/MessageList/messageStyles';
import * as actions from '../../../store/actions/index';

const ProductCreateForm = ({ handleSubmit }) => {
    const history = useHistory();
    const [error, setError] = useState({});

    const dispatch = useDispatch();
    const onShowSpinner = () => dispatch(actions.showSpinner());
    const onHideSpinner = () => dispatch(actions.hideSpinner());
    const onMessage = (style, message) => dispatch(actions.message(style, message));
    const axios = useAxios();

    const onSubmitForm = async (form) => {
        onShowSpinner();
        const path = 'products';
        const body = { ...form };

        await axios.post(path, body)
            .then(() => {
                history.push('/products');
            }).catch((errors) => {
                switch (errors.response.status) {
                    case 422:
                        setError(errors.response.data.errors);
                        break;
                    default:
                        onMessage(MS.ERROR, 'Ismeretlen hiba történt');
                }
            }).finally(() => {
                onHideSpinner();
            });
    };

    return (
        <form className="section" onSubmit={handleSubmit(onSubmitForm)}>
            <ProductForm error={error} />
        </form>
    );
};

export default reduxForm({
    form: 'product_create_form'
})(ProductCreateForm);