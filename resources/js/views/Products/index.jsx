import React, { useCallback, useEffect, useState } from 'react';
import { Link, useHistory } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import ProductFilter from './components/ProductFilter';
import ProductListItem from './components/ProductListItem';
import useAxios from '../../axios';
import * as MS from '../../components/MessageList/messageStyles';
import Paginator from '../../components/Paginator/Paginator';
import * as actions from '../../store/actions/index';

const Product = () => {
    const history = useHistory();
    const [rows, setRows] = useState([]);
    const [count, setCount] = useState(0);

    const paginatorQuerys = [
        { key: 'page', value: 1 },
        { key: 'rows-per-page', value: 10 }
    ];

    const filterQuerys = [
        { key: 'order', value: 'asc' },
        { key: 'order-by', value: 'date' }
    ];

    const dispatch = useDispatch();
    const onShowSpinner = () => dispatch(actions.showSpinner());
    const onHideSpinner = () => dispatch(actions.hideSpinner());
    const onMessage = (style, message) => dispatch(actions.message(style, message));
    const axios = useAxios();

    const load = useCallback(async () => {
        onShowSpinner();
        await axios.get(`products${window.location.search}`)
            .then((response) => {
                setRows(response.data.list);
                setCount(response.data.count);
            }).catch(() => {
                onMessage(MS.ERROR, 'Ismeretlen hiba történt');
            }).finally(() => {
                onHideSpinner();
            });
    }, [onShowSpinner, onHideSpinner]);

    useEffect(() => {
        if (window.location.search.length > 0) {
            load();
        }
    }, [window.location.search]);

    useEffect(() => {
        if (window.location.search.length > 0) {
            return;
        }

        let url = window.location.pathname;
        url += `?${paginatorQuerys.map((query) => `${query.key}=${query.value}`).join('&')}`;
        url += `&${filterQuerys.map((query) => `${query.key}=${query.value}`).join('&')}`;

        history.push(url);
    }, []);

    return (
        <>
            <div className="section-header">
                <div className="section-header__title">Termék lista</div>
                <Link to="/products/create" className="btn btn--purple">Új termék</Link>
            </div>
            <div className="section">
                {rows.length > 0 && <ProductFilter />}
                {rows.map((row) => (
                    <ProductListItem key={row.data.product_id} data={row.data} load={load} />
                ))}
                {rows.length > 0 && <Paginator rowNum={count} />}
            </div>
        </>
    );
};

export default Product;