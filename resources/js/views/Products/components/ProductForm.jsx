import React from 'react';
import { Link } from 'react-router-dom';
import { Field } from 'redux-form';
import { Group, Row } from '../../../components/Form/Form';

const ProductForm = ({ error }) => {
    return (
        <>
            <Row>
                <Group col="6">
                    <label htmlFor="name" className="section__label">Name</label>
                    <Field name="name" component="input" type="text" className="section__input" />
                    {error.name && <p className="section__error">{error.name}</p>}
                </Group>
                <Group col="6">
                    <label htmlFor="price" className="section__label">Price</label>
                    <Field name="price" component="input" type="text" className="section__input" />
                    {error.price && <p className="section__error">{error.price}</p>}
                </Group>
            </Row>
            <Row>
                <Group col="12">
                    <label htmlFor="description" className="section__label">Description</label>
                    <Field name="description" component="textarea" rows="8" className="section__textarea" />
                    {error.description && <p className="section__error">{error.description}</p>}
                </Group>
            </Row>
            <Row>
                <button type="submit" className="btn btn--purple">Mentés</button>
                <Link to="/products" className="btn btn--gray">Vissza</Link>
            </Row>
        </>
    );
};

export default ProductForm;