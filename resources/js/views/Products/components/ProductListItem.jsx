import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import useAxios from '../../../axios';
import Cross from '../../../components/Icons/Cross';
import Pencil from '../../../components/Icons/Pencil';
import * as MS from '../../../components/MessageList/messageStyles';
import ConfirmPopup from '../../../components/Popup/ConfirmPopup';
import { toHuf } from '../../../helpers/moneyFormatHelper';
import * as actions from '../../../store/actions/index';

const ProductListItem = ({ data, load }) => {
    const [showPopup, setShowPopup] = useState(false);

    const dispatch = useDispatch();
    const onMessage = (style, message) => dispatch(actions.message(style, message));
    const axios = useAxios();

    const onDelete = async () => {
        setShowPopup(false);
        await axios.delete(`products/${data.product_id}`)
            .then(() => {
                onMessage(MS.SUCCESS, `A(z) ${data.name} terméket törlése sikeresen megtörtént`);
                load();
            }).catch(() => {
                onMessage(MS.ERROR, 'Ismeretlen hiba történt');
            });
    };

    const onClose = () => {
        setShowPopup(false);
    };

    const operations = {
        confirm: {
            label: 'Törlés',
            callbacks: onDelete
        },
        cancel: {
            label: 'Mégse',
            callbacks: onClose
        }
    }

    return (
        <div className="list-item">
            {showPopup &&
                <ConfirmPopup
                    header="Felhasználó törlése"
                    body={`Biztosan törölni szeretné a(z) ${data.name} terméket?`}
                    operations={operations}
                />
            }

            <div className="list-item__marker">
                <div className="list-item__marker-dot"></div>
            </div>

            <div className="list-item__data-group">
                <p>{data.name}</p>
                <p>{toHuf(data.price)}</p>
                <p>{data.created_at}</p>
            </div>

            <div className="list-item__action-group list-item__action-group--2">
                <Link to={`/products/edit/${data.product_id}`} className="list-item__btn list-item__btn--purple">
                    <Pencil className="list-item__btn-icon" />
                    <span className="tooltip">Szerkesztés</span>
                </Link>
                <Link to={`/products${window.location.search}`} className="list-item__btn list-item__btn--red" onClick={() => setShowPopup(true)}>
                    <Cross className="list-item__btn-icon" />
                    <span className="tooltip">Törlés</span>
                </Link>
            </div>
        </div>
    );
};

export default ProductListItem;