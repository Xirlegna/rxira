import React, { useCallback, useEffect, useState } from 'react';
import { useParams } from 'react-router';
import { useDispatch } from 'react-redux';
import ProductEditForm from './ProductEditForm';
import useAxios from '../../../axios';
import * as MS from '../../../components/MessageList/messageStyles';
import * as actions from '../../../store/actions/index';

const ProductEdit = () => {
    const { id } = useParams();
    const [params, setParams] = useState({});

    const dispatch = useDispatch();
    const onShowSpinner = () => dispatch(actions.showSpinner());
    const onHideSpinner = () => dispatch(actions.hideSpinner());
    const onMessage = (style, message) => dispatch(actions.message(style, message));
    const axios = useAxios();

    const load = useCallback(async () => {
        onShowSpinner();
        await axios.get(`products/${id}`)
            .then((response) => {
                setParams({
                    name: response.data.data.name,
                    price: response.data.data.price,
                    description: response.data.data.description
                });
            })
            .catch(() => {
                onMessage(MS.ERROR, 'Ismeretlen hiba történt');
            }).finally(() => {
                onHideSpinner();
            });
    }, [id]);

    useEffect(() => { load(); }, [load]);

    return (
        <>
            <div className="section-header">
                <div className="section-header__title">Product Create</div>
            </div>
            <ProductEditForm initialValues={params} />
        </>
    );
};

export default ProductEdit;