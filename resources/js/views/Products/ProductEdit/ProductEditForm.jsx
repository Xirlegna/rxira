import React, { useState } from 'react';
import { useParams } from 'react-router';
import { useHistory } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { reduxForm } from 'redux-form';
import ProductForm from '../components/ProductForm';
import useAxios from '../../../axios';
import * as MS from '../../../components/MessageList/messageStyles';
import * as actions from '../../../store/actions/index';

const ProductEditForm = ({ handleSubmit }) => {
    const history = useHistory();
    const { id } = useParams();
    const [error, setError] = useState({});

    const dispatch = useDispatch();
    const onShowSpinner = () => dispatch(actions.showSpinner());
    const onHideSpinner = () => dispatch(actions.hideSpinner());
    const onMessage = (style, message) => dispatch(actions.message(style, message));
    const axios = useAxios();

    const onSubmitForm = async (form) => {
        onShowSpinner();
        const path = `products/${id}`;
        const body = { ...form };

        await axios.patch(path, body)
            .then(() => {
                history.push("/products");
            }).catch((errors) => {
                switch (errors.response.status) {
                    case 422:
                        setError(errors.response.data.errors);
                        break;
                    default:
                        onMessage(MS.ERROR, 'Ismeretlen hiba történt');
                }
            }).finally(() => {
                onHideSpinner();
            });
    };

    return (
        <form className="section" onSubmit={handleSubmit(onSubmitForm)}>
            <ProductForm error={error} />
        </form>
    );
};

export default reduxForm({
    form: 'product_edit_form',
    enableReinitialize: true
})(ProductEditForm);