import React, { useState } from 'react';
import { Field, reduxForm } from 'redux-form';
import { Link, useHistory } from 'react-router-dom';
import useAxios from '../axios';
import { Group, Row } from '../components/Form/Form';
import PasswordInput from '../components/Form/Input/PasswordInput';

const Register = ({ handleSubmit }) => {
    const history = useHistory();
    const axios = useAxios();
    const [error, setError] = useState({});

    const onSubmitForm = async (form) => {
        const path = 'register';
        const body = { ...form };

        await axios.post(path, body)
            .then(() => {
                history.push("/");
            }).catch((errors) => {
                setError(errors.response.data.errors);
            });
    };

    return (
        <form className="section" onSubmit={handleSubmit(onSubmitForm)}>
            <Row>
                <Group col="12">
                    <label htmlFor="name" className="section__label">Name</label>
                    <Field name="name" component="input" type="text" className="section__input" />
                    {error.name && <p className="section__error">{error.name}</p>}
                </Group>
            </Row>
            <Row>
                <Group col="12">
                    <label htmlFor="email" className="section__label">E-mail</label>
                    <Field name="email" component="input" type="email" className="section__input" />
                    {error.email && <p className="section__error">{error.email}</p>}
                </Group>
            </Row>
            <Row>
                <Group col="12">
                    <Field
                        label="Password"
                        name="password"
                        error={error.password}
                        component={PasswordInput}
                    />
                </Group>
            </Row>
            <Row>
                <Group col="12">
                    <Field
                        label="Password Confirmation"
                        name="password_confirmation"
                        component={PasswordInput}
                    />
                </Group>
            </Row>
            <Row>
                <button type="submit" className="btn btn--purple">Elfogad</button>
                <Link to="/" className="btn btn--gray">Login</Link>
            </Row>
        </form>
    );
};

export default reduxForm({ form: 'register_form' })(Register);