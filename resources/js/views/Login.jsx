import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Field, reduxForm } from 'redux-form';
import { Link } from 'react-router-dom';

import { Group, Row } from '../components/Form/Form';
import EmailInput from '../components/Form/Input/EmailInput';
import PasswordInput from '../components/Form/Input/PasswordInput';
import * as MS from '../components/MessageList/messageStyles';
import * as actions from '../store/actions/index';

const Login = ({ handleSubmit }) => {
    const [error, setError] = useState({});

    const dispatch = useDispatch();
    const onLogin = (form) => dispatch(actions.login(form));
    const onMessage = (style, message) => dispatch(actions.message(style, message));

    const auth = useSelector((state) => state.auth);

    useEffect(() => {
        setError(auth.errors);
    }, [auth]);

    const onSubmitForm = async (form) => {
        onLogin(form);
    };

    useEffect(() => {
        error.general && onMessage(MS.ERROR, error.general);
    }, [error]);

    return (
        <form className="section" onSubmit={handleSubmit(onSubmitForm)}>
            <Row>
                <Group col="12">
                    <Field
                        label="E-mail"
                        name="email"
                        error={error.email}
                        component={EmailInput}
                    />
                </Group>
            </Row>
            <Row>
                <Group col="12">
                    <Field
                        label="Password"
                        name="password"
                        error={error.password}
                        component={PasswordInput}
                    />
                </Group>
            </Row>
            <Row>
                <button type="submit" className="btn btn--purple">Elfogad</button>
                <Link to="/register" className="btn btn--gray">Register</Link>
            </Row>
        </form>
    );
};

export default reduxForm({ form: 'login_form' })(Login);