import React from 'react';
import { useDispatch } from 'react-redux';
import * as MS from '../components/MessageList/messageStyles';
import * as actions from '../store/actions/index';

const Home = () => {
    const dispatch = useDispatch();
    const onMessage = (style, message) => dispatch(actions.message(style, message));

    return (
        <>
            <div className="section-header">
                <div className="section-header__title">Page title</div>
            </div>
            <button type="submit" className="btn btn--purple" onClick={() => onMessage(MS.ERROR, `Test message ${new Date().getTime()}`)}>Add message</button>
        </>
    );
};

export default Home;