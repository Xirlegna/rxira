import parseUrl from './parseUrlHelper';

const setUp = () => {
    const urlParams = parseUrl();

    let page = urlParams.find((param) => param.key === 'page');
    page = page ? +page.value : 1;

    let rowsPerPage = urlParams.find((param) => param.key === 'rows-per-page');
    rowsPerPage = rowsPerPage ? +rowsPerPage.value : 10;

    return { page, rowsPerPage }
};

export default setUp;