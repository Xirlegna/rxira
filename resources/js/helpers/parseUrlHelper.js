const parseUrl = () => {
    const urlParams = [];

    if (window.location.search.length <= 0) {
        return urlParams; 
    }

    window.location.search
        .slice(1)
        .split('&')
        .forEach((param) => {
            const [key, value] = param.split('=');
            urlParams.push({ key, value });
        });

    return urlParams;
}

export default parseUrl;