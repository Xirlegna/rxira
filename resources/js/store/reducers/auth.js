import * as actionTypes from '../actions/actionTypes';

const initialState = {
    id: localStorage.getItem('id'),
    token: localStorage.getItem('token'),
    role: localStorage.getItem('role'),
    name: localStorage.getItem('name'),
    loading: false,
    errors: {}
};

const loginStart = (state) => {
    return {
        ...state,
        loading: true
    };
};

const loginSuccess = (state, action) => {
    localStorage.setItem('id', action.data.id);
    localStorage.setItem('token', action.data.token);
    localStorage.setItem('role', action.data.role);
    localStorage.setItem('name', action.data.name);
    return {
        ...state,
        id: action.data.id,
        token: action.data.token,
        role: action.data.role,
        name: action.data.name,
        loading: false
    }
};

const loginFail = (state, action) => {
    localStorage.removeItem('id');
    localStorage.removeItem('token');
    localStorage.removeItem('role');
    localStorage.removeItem('name');
    return {
        ...state,
        errors: action.data,
        loading: false
    }
};

const logoutStart = (state) => {
    return {
        ...state,
        loading: true
    };
};

const logout = (state) => {
    localStorage.removeItem('id');
    localStorage.removeItem('token');
    localStorage.removeItem('role');
    localStorage.removeItem('name');
    return {
        ...state,
        token: '',
        loading: false,
        errors: {}
    }
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.LOGIN_START: return loginStart(state);
        case actionTypes.LOGIN_SUCCESS: return loginSuccess(state, action);
        case actionTypes.LOGIN_FAIL: return loginFail(state, action);
        case actionTypes.LOGOUT_START: return logoutStart(state);
        case actionTypes.LOGOUT_SUCCESS: return logout(state);
        case actionTypes.LOGOUT_FAIL: return logout(state);
        default: return state;
    }
};

export default reducer;