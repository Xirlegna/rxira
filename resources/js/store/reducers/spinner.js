import * as actionTypes from '../actions/actionTypes';

const initialState = {
    isShow: false
};

const showSpinner = (state) => {
    return {
        ...state,
        isShow: true
    }
}

const hideSpinner = (state) => {
    return {
        ...state,
        isShow: false
    }
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.SHOW_SPINNER: return showSpinner(state);
        case actionTypes.HIDE_SPINNER: return hideSpinner(state);
        default: return state;
    }
};

export default reducer;