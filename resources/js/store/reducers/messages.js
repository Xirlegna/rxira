import { list } from 'postcss';
import * as actionTypes from '../actions/actionTypes';

const initialState = {
    list: []
};

const addMessage = (state, action) => {
    return {
        ...state,
        list: [...state.list, { style: action.style, label: action.label }]
    };
};

const removeMessage = (state) => {
    const list = [...state.list];
    list.shift();

    return {
        ...state,
        list
    };
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.ADD_MESSAGE: return addMessage(state, action);
        case actionTypes.REMOVE_MESSAGE: return removeMessage(state);
        default: return state;
    }
};

export default reducer;