import * as actionTypes from './actionTypes';

export const addMessage = (style, label) => {
    return {
        type: actionTypes.ADD_MESSAGE,
        style,
        label
    };
};

export const removeMessage = () => {
    return {
        type: actionTypes.REMOVE_MESSAGE
    };
};

export const message = (style, label) => {
    return (dispatch) => {
        dispatch(addMessage(style, label));

        setTimeout(() => {
            dispatch(removeMessage());
        }, 4000);
    };
}