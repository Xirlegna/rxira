import * as actionTypes from './actionTypes';
import useAxios from '../../axios';

export const loginStart = () => {
    return {
        type: actionTypes.LOGIN_START
    };
};

export const loginSuccess = (data) => {
    return {
        type: actionTypes.LOGIN_SUCCESS,
        data
    };
};

export const loginFail = (errors) => {
    return {
        type: actionTypes.LOGIN_FAIL,
        data: errors
    };
};

export const logoutStart = () => {
    return {
        type: actionTypes.LOGOUT_START
    };
};

export const logoutSuccess = () => {
    return {
        type: actionTypes.LOGOUT_SUCCESS,
    };
};

export const logoutFail = () => {
    return {
        type: actionTypes.LOGOUT_FAIL,
    };
};

export const login = (form) => {
    const axios = useAxios();

    return (dispatch) => {
        dispatch(loginStart());

        axios.post('login', form)
            .then((response) => {
                dispatch(loginSuccess({
                    id: response.data.user.id,
                    token: response.data.token,
                    role: response.data.user.role,
                    name: response.data.user.name
                }));
            }).catch((errors) => {
                switch (errors.response.status) {
                    case 422:
                        dispatch(loginFail(errors.response.data.errors));
                        break;
                    default:
                        dispatch(loginFail({ general: 'Hibás e-mail cím vagy jelszó' }));
                }
            });
    };
};

export const logout = () => {
    const axios = useAxios();

    return (dispatch) => {
        dispatch(logoutStart());

        axios.post('logout')
            .then(() => {
                dispatch(logoutSuccess());
            }).catch(() => {
                dispatch(logoutFail());
            });
    };
};