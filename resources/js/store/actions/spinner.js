import * as actionTypes from './actionTypes';

export const showSpinner = () => {
    return {
        type: actionTypes.SHOW_SPINNER
    };
};

export const hideSpinner = () => {
    return {
        type: actionTypes.HIDE_SPINNER
    };
};
