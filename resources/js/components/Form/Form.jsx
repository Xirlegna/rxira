import React from 'react';

export const Row = ({ children }) => (
    <div className="section__row">{children}</div>
);

export const Group = ({ col, children }) => (
    <div className={['section__group', `section__group--col${col}`].join(' ')}>
        {children}
    </div>
);