import React from 'react';
import PropTypes from 'prop-types';

const EmailInput = ({ input, label, error }) => (
    <>
        <label htmlFor={input.name} className="section__label">{label}</label>
        <input {...input} type="email" className="section__input" />
        {error && <p className="section__error">{error}</p>}
    </>
);

EmailInput.propTypes = {
    input: PropTypes.shape({
        name: PropTypes.string,
    }).isRequired,
    label: PropTypes.string.isRequired,
    error: PropTypes.array
};

EmailInput.defaultProps = {
    error: []
};

export default EmailInput;