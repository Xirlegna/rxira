import React, { useState } from 'react';
import PropTypes from 'prop-types';
import Eye from '../../Icons/Eye';
import EyeBlocked from '../../Icons/EyeBlocked';

const PasswordInput = ({ input, label, error }) => {
    const [type, setType] = useState('password');

    const changeType = () => {
        setType(type === 'password' ? 'text' : 'password');
    }

    return (
        <>
            <label htmlFor={input.name} className="section__label">{label}</label>
            <div className="input-with-action">
                <input {...input} type={type} className="input-with-action__input" autoComplete="off" />
                <div className="input-with-action__btn" onClick={() => changeType()}>
                    {
                        type === 'password'
                            ? <Eye className="input-with-action__icon" />
                            : <EyeBlocked className="input-with-action__icon" />
                    }
                </div>
            </div>
            {error && <p className="section__error">{error}</p>}
        </>
    );
};

PasswordInput.propTypes = {
    input: PropTypes.shape({
        name: PropTypes.string,
    }).isRequired,
    label: PropTypes.string.isRequired,
    error: PropTypes.array
};

PasswordInput.defaultProps = {
    error: []
};

export default PasswordInput;