import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import ArrowLeft from '../Icons/ArrowLeft';
import ArrowRight from '../Icons/ArrowRight';
import parseUrl from '../../helpers/parseUrlHelper';

const Paginator = ({ rowNum }) => {
    const history = useHistory();

    const [querys, setQuerys] = useState([]);
    const [page, setPage] = useState(0);
    const [pageBtns, setPageBtns] = useState([]);
    const [rowsPerPage, setRowsPerPage] = useState(0);
    const [max, setMax] = useState(0);

    useEffect(() => {
        const urlParams = parseUrl();

        const pageObj = urlParams.find((param) => param.key === 'page');
        const rowsPerPageObj = urlParams.find((param) => param.key === 'rows-per-page');

        setQuerys(urlParams.filter((param) => !['page', 'rows-per-page'].includes(param.key)));
        setPage(pageObj ? +pageObj.value : 1);
        setRowsPerPage(rowsPerPageObj ? +rowsPerPageObj.value : 10);
    }, []);

    useEffect(() => {
        if (querys.length <= 0) {
            return;
        }

        const btnNumbers = Math.ceil(rowNum / rowsPerPage);
        const btns = [];
        let actPage = page;

        if (page > btnNumbers) {
            actPage = btnNumbers;
        }

        if (actPage < 1) {
            actPage = 1;
        }

        for (let i = 1; i <= btnNumbers; i++) {
            const classes = ['paginator__nav-body-btn'];
            if (i === actPage) {
                classes.push('paginator__nav-body-btn--active');
            }
            btns.push({ number: i, classes });
        }

        setMax(btnNumbers);
        setPageBtns(btns);

        const urlParams = parseUrl();
        const currentQuerys = urlParams.filter((param) => !['page', 'rows-per-page'].includes(param.key));

        let url = `${window.location.pathname}?page=${actPage}&rows-per-page=${rowsPerPage}`;
        url += `&${currentQuerys.map((query) => `${query.key}=${query.value}`).join('&')}`;

        history.push(url);
    }, [querys, page, rowsPerPage]);

    const onPrev = () => {
        if (page > 1) {
            const newPage = page - 1;

            setPage(newPage);
        }
    }

    const onNext = () => {
        if (page < max) {
            const newPage = page + 1;

            setPage(newPage);
        }
    }

    return (
        <div className="paginator">
            <div className="paginator__nav">
                <select
                    className="paginator__nav-select"
                    value={rowsPerPage}
                    onChange={(e) => setRowsPerPage(e.target.value)}
                >
                    <option value="2">2</option>
                    <option value="5">5</option>
                    <option value="10">10</option>
                </select>
                <div className="paginator__nav-btn" onClick={() => onPrev()}>
                    <ArrowLeft className="paginator__nav-btn-icon" />
                </div>
                <div className="paginator__nav-body">
                    {pageBtns.map((btn) => (
                        <p
                            key={btn.number}
                            className={btn.classes.join(' ')}
                            onClick={() => setPage(btn.number)}
                        >
                            {btn.number}
                        </p>
                    ))}
                </div>
                <div className="paginator__nav-btn" onClick={() => onNext()}>
                    <ArrowRight className="paginator__nav-btn-icon" />
                </div>
            </div>
        </div>
    );
}

export default Paginator;