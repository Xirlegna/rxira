import React from 'react';
import { Route, Switch } from 'react-router-dom';
import UserList from '../../views/Users/index';
import UserCreate from '../../views/Users/UserCreate/index';
import UserEdit from '../../views/Users/UserEdit/index';

const UserRoute = () => (
    <Switch>
        <Route exact path="/users" component={UserList} />
        <Route path="/users/create" component={UserCreate} />
        <Route path="/users/edit/:id" component={UserEdit} />
    </Switch>
);

export default UserRoute;