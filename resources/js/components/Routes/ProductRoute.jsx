import React from 'react';
import { Route, Switch } from 'react-router-dom';
import ProductList from '../../views/Products/index';
import ProductCreate from '../../views/Products/ProductCreate/index';
import ProductEdit from '../../views/Products/ProductEdit/index';

const ProductRoute = () => (
    <Switch>
        <Route exact path="/products" component={ProductList} />
        <Route path="/products/create" component={ProductCreate} />
        <Route path="/products/edit/:id" component={ProductEdit} />
    </Switch>
);

export default ProductRoute;