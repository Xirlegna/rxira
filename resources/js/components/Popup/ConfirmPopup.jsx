import React from 'react';
import Backdrop from '../Backdrop/Backdrop';

const ConfirmPopup = ({ header, body, operations }) => {
    return (
        <Backdrop>
            <div className="popup">
                <p className="popup__header">{header}</p>
                <p className="popup__body">{body}</p>
                <div className="popup__footer">
                    <button
                        type="button"
                        className="btn btn--red"
                        onClick={() => operations.confirm.callbacks()}
                    >
                        {operations.confirm.label}
                    </button>
                    <button
                        type="button"
                        className="btn btn--gray"
                        onClick={() => operations.cancel.callbacks()}
                    >
                        {operations.cancel.label}
                    </button>
                </div>
            </div>
        </Backdrop>
    );
};

export default ConfirmPopup;