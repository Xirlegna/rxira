import React from 'react';
import { useSelector } from 'react-redux';
import MessageList from '../MessageList/MessageList';

const AuthLayout = (props) => {
    const messages = useSelector((state) => state.message.list);

    return (
        <div className="auth-main">
            {messages.length > 0 && <MessageList />}
            {props.children}
        </div>
    );
};

export default AuthLayout;