import React from 'react';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import Spinner from '../Spinner/Spinner';
import UserBtn from '../UserBtn/UserBtn';
import MessageList from '../MessageList/MessageList';

const Layout = (props) => {
    const auth = useSelector((state) => state.auth);
    const messages = useSelector((state) => state.message.list);
    const spinner = useSelector((state) => state.spinner);

    return (
        <>
            <header className="header">
                <UserBtn />
            </header>

            <aside className="sidenav">
                <ul className="sidenav__list">
                    <li className="sidenav__list-item">
                        <Link to="/" className="sidenav__list-link">Home</Link>
                    </li>
                    <li className="sidenav__list-item">
                        <Link to="/products" className="sidenav__list-link">Product</Link>
                    </li>
                    {auth.role === 'SUEPR_ADMIN' && (
                        <li className="sidenav__list-item">
                            <Link to="/users" className="sidenav__list-link">User</Link>
                        </li>
                    )}
                </ul>
            </aside>

            <main className="main">
                {spinner.isShow && <Spinner label="Betöltés" />}
                {messages.length > 0 && <MessageList />}
                {props.children}
            </main>

            <footer className="footer">
                <div className="footer__copyright">&copy; 2021</div>
                <div className="footer__signature">Made with love by pure genius</div>
            </footer>
        </>
    );
};

export default Layout;