import React from 'react';
import Warning from '../../Icons/Warning';

const SuccessMessage = ({ message }) => (
    <div className="message message--success">
        <Warning className="message__icon" />
        <p>{message}</p>
    </div>
);

export default SuccessMessage;