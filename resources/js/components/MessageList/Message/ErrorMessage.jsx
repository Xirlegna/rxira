import React from 'react';
import Warning from '../../Icons/Warning';

const ErrorMessage = ({ message }) => (
    <div className="message message--error">
        <Warning className="message__icon" />
        <p>{message}</p>
    </div>
);

export default ErrorMessage;