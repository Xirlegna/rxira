import React from 'react';
import { v4 as uuidv4 } from 'uuid';
import { useSelector } from 'react-redux';
import * as MS from './messageStyles';
import ErrorMessage from './Message/ErrorMessage';
import SuccessMessage from './Message/SuccessMessage';

const MessageList = () => {
    const messages = useSelector((state) => state.message.list);

    return (
        <div className="message-list">
            {messages.map((message) => {
                switch (message.style) {
                    case MS.ERROR:
                        return <ErrorMessage key={uuidv4()} message={message.label} />;
                    case MS.SUCCESS:
                        return <SuccessMessage key={uuidv4()} message={message.label} />;
                }

            })}
        </div>
    );
};

export default MessageList;