import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import ArrowDown from '../Icons/ArrowDown';
import * as actions from '../../store/actions/index';

const UserBtn = () => {
    const dispatch = useDispatch();
    const onLogout = () => dispatch(actions.logout());
    const auth = useSelector((state) => state.auth);

    const [show, setShow] = useState(false);

    const logout = () => {
        onLogout();
    };

    const handleClick = () => {
        if (show) {
            setShow(false);
        }
    };

    useEffect(() => {
        window.addEventListener('click', handleClick);

        return () => {
            window.removeEventListener('click', handleClick);
        };
    }, [show]);

    return (
        <>
            <div className="user-btn" onClick={() => setShow(!show)}>
                <div className="user-btn__circle">
                    {
                        auth.name.length > 0 && auth.name.toUpperCase().match(/\b(\w)/g).join('').slice(0, 2)
                    }
                </div>
                <ArrowDown className="user-btn__arrow" />
            </div>
            {
                show && (
                    <div className="user-btn-dropdown">
                        <ul className="user-btn-dropdown__list">
                            <li className="user-btn-dropdown__list-item">
                                <Link
                                    to="/user-custom"
                                    className="user-btn-dropdown__list-link"
                                >
                                    Profil szerkesztés
                                </Link>
                            </li>
                            <li className="user-btn-dropdown__list-item">
                                <Link
                                    to="/users/change-password"
                                    className="user-btn-dropdown__list-link"
                                >
                                    Jelszó változtatás
                                </Link>
                            </li>
                            <li className="user-btn-dropdown__list-item" onClick={() => logout()}>
                                <Link
                                    to={`${window.location.pathname}${window.location.search}`}
                                    className="user-btn-dropdown__list-link"
                                >
                                    Kijelentkezés
                                </Link>
                            </li>
                        </ul>
                    </div>
                )
            }
        </>
    );
};

export default UserBtn;
