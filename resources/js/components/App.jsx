import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Route, Switch, useHistory, withRouter } from 'react-router-dom';
import AuthLayout from './Layout/AuthLayout';
import Layout from './Layout/Layout';
import ProductRoute from './Routes/ProductRoute';
import UserRoute from './Routes/UserRoute';
import UserChangePassword from '../views/Users/UserChangePassword/index';
import UserCustom from '../views/Users/UserCustom/index';
import Home from '../views/Home';
import Login from '../views/Login';
import Register from '../views/Register';
import * as actions from '../store/actions/index';

const App = () => {
    const history = useHistory();
    const dispatch = useDispatch();
    const [login, setLogin] = useState(false);
    const auth = useSelector((state) => state.auth);

    const onShowSpinner = () => dispatch(actions.showSpinner());
    const onHideSpinner = () => dispatch(actions.hideSpinner());

    useEffect(() => {
        setLogin(auth.token?.length > 0);
        if (auth.token?.length === 0) {
            history.push('/');
        }
    }, [auth]);

    useEffect(() => {
        auth.loading ? onShowSpinner() : onHideSpinner();
    }, [auth.loading]);

    const authLayout = (
        <AuthLayout>
            <Switch>
                <Route path="/" exact component={Login} />
                <Route path="/register" component={Register} />
            </Switch>
        </AuthLayout>
    );

    const appLayout = (
        <Layout>
            <Switch>
                <Route path="/" exact component={Home} />
                <Route path="/users/change-password" component={UserChangePassword} />
                <Route path="/user-custom" exact component={UserCustom} />
                {auth.role === 'SUEPR_ADMIN' && (
                    <Route path="/users" component={UserRoute} />
                )}
                <Route path="/products" component={ProductRoute} />
            </Switch>
        </Layout>
    );

    return (
        <>
            {login ? appLayout : authLayout}
        </>
    );
};

export default withRouter(App);