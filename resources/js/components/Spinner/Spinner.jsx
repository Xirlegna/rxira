import React from 'react';
import Backdrop from '../Backdrop/Backdrop';

const Spinner = ({ label }) => (
    <Backdrop>
        <div className="spinner">{label}</div>
    </Backdrop>
);

export default Spinner;