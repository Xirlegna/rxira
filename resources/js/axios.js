import axios from 'axios';

const useAxios = () => {
    return axios.create({
        headers: {
            common: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`,
                'Accept': 'application/json'
            }
        },
        baseURL: '/api/'
    });
};

export default useAxios;