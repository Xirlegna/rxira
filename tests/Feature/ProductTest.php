<?php

namespace Tests\Feature;

use App\Models\Product;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;
use Tests\Traits\UserTraits;

class ProductTest extends TestCase
{
    use RefreshDatabase;
    use UserTraits;

    public function setUp(): void
    {
        parent::setUp();
        $this->seed();
        $this->setUpUser();
    }

    public function testCanListProducts()
    {
        $response = $this
            ->withHeaders(['Authorization' => 'Bearer ' . $this->sadminToken])
            ->get('/api/products');

        $response->assertStatus(Response::HTTP_OK);
        $response->assertJsonCount(2);
        $this->assertEquals(4, json_decode($response->getContent())->count);
    }

    public function testCanStoreProduct()
    {
        $response = $this
            ->withHeaders(['Authorization' => 'Bearer ' . $this->sadminToken])
            ->post('/api/products', $this->getData());

        $response->assertStatus(Response::HTTP_CREATED);
        $response->assertJson([
            'data' => $this->getData()
        ]);
    }

    public function testCanShowProduct()
    {
        $product = $this->getProductBySlug('product-a');

        $response = $this
            ->withHeaders(['Authorization' => 'Bearer ' . $this->sadminToken])
            ->get('/api/products/' . $product->id);

        $response->assertJson([
            'data' => [
                'name' => $product->name,
                'slug' => $product->slug,
                'price' => $product->price,
                'description' => $product->description
            ]
        ]);
    }

    public function testCanUpdateProduct()
    {
        $product = $this->getProductBySlug('product-a');

        $response = $this
            ->withHeaders(['Authorization' => 'Bearer ' . $this->sadminToken])
            ->patch('/api/products/' . $product->id, [
                'name' => 'New Product A',
                'price' => $product->price,
                'description' => 'New Product A desc'
            ]);

        $response->assertStatus(Response::HTTP_OK);
        $response->assertJson([
            'data' => [
                'name' => 'New Product A',
                'slug' => 'new-product-a',
                'description' => 'New Product A desc'
            ]
        ]);
    }

    public function testCanDestroyProduct()
    {
        $product = $this->getProductBySlug('product-a');

        $response = $this
            ->withHeaders(['Authorization' => 'Bearer ' . $this->sadminToken])
            ->delete('/api/products/' . $product->id);

        $this->assertCount(3, Product::all());
        $response->assertStatus(Response::HTTP_NO_CONTENT);
    }

    public function testProductFieldsAreRequired()
    {
        collect(['name', 'price', 'description'])->each(function ($field) {
            $base = Product::all();

            $response = $this
                ->withHeaders(['Authorization' => 'Bearer ' . $this->sadminToken])
                ->post('/api/products', array_merge($this->getData(), [$field => '']));

            $response->assertSessionHasErrors($field);
            $this->assertCount(count($base), Product::all());
        });
    }

    public function testProductFieldsAreInteger()
    {
        collect(['price'])->each(function ($field) {
            $base = Product::all();

            $response = $this
                ->withHeaders(['Authorization' => 'Bearer ' . $this->sadminToken])
                ->post('/api/products', array_merge($this->getData(), [$field => 'abc']));

            $response->assertSessionHasErrors($field);
            $this->assertCount(count($base), Product::all());
        });
    }

    private function getData()
    {
        return [
            'name' => 'Test product',
            'price' => 9999,
            'description' => 'Test description'
        ];
    }

    private function getProductBySlug(String $slug): Product
    {
        return Product::where('slug', $slug)->first();
    }
}
