<?php

namespace Tests\Feature;

use App\Models\Role;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;
use Tests\Traits\UserTraits;

class UserAdminTest extends TestCase
{
    use RefreshDatabase;
    use UserTraits;

    public function setUp(): void
    {
        parent::setUp();
        $this->seed();
        $this->setUpUser();
    }

    public function testAdminCanNotListUsers()
    {
        $response = $this
            ->withHeaders(['Authorization' => 'Bearer ' . $this->adminToken])
            ->get('/api/users');

        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    public function testAdminCanNotStoreUser()
    {
        $response = $this
            ->withHeaders(['Authorization' => 'Bearer ' . $this->adminToken])
            ->post('/api/users', $this->getData());

        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    public function testAdminCanNotShowAnotherUser()
    {
        $user = $this->getUserByEmail('another.general.user@mail.com');

        $response = $this
            ->withHeaders(['Authorization' => 'Bearer ' . $this->adminToken])
            ->get('/api/users/' . $user->id);

        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    public function testAdminCanNotUpdateAnotherUser()
    {
        $user = $this->getUserByEmail('another.general.user@mail.com');

        $response = $this
            ->withHeaders(['Authorization' => 'Bearer ' . $this->adminToken])
            ->patch('/api/users/' . $user->id, [
                'name' => 'New Test User',
                'email' => $user->email,
                'role' => Role::ADMIN
            ]);

        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    public function testAdminCanNotDestroyAnotherUser()
    {
        $user = $this->getUserByEmail('another.general.user@mail.com');

        $response = $this
            ->withHeaders(['Authorization' => 'Bearer ' . $this->adminToken])
            ->delete('/api/users/' . $user->id);

        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    private function getData()
    {
        return [
            'name' => 'Test User',
            'email' => 'test.user@email.com',
            'password' => 'Wasd1234',
            'role' => Role::GENERAL
        ];
    }
}
