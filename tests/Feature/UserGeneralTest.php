<?php

namespace Tests\Feature;

use App\Models\Role;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;
use Tests\Traits\UserTraits;

class UserGeneralTest extends TestCase
{
    use RefreshDatabase;
    use UserTraits;

    public function setUp(): void
    {
        parent::setUp();
        $this->seed();
        $this->setUpUser();
    }

    public function testGeneralCanNotListUsers()
    {
        $response = $this
            ->withHeaders(['Authorization' => 'Bearer ' . $this->generalToken])
            ->get('/api/users');

        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    public function testGeneralCanNotStoreUser()
    {
        $response = $this
            ->withHeaders(['Authorization' => 'Bearer ' . $this->generalToken])
            ->post('/api/users', $this->getData());

        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    public function testGeneralCanNotShowAnotherUser()
    {
        $user = $this->getUserByEmail('another.general.user@mail.com');

        $response = $this
            ->withHeaders(['Authorization' => 'Bearer ' . $this->generalToken])
            ->get('/api/users/' . $user->id);

        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    public function testGeneralCanNotUpdateAnotherUser()
    {
        $user = $this->getUserByEmail('another.general.user@mail.com');

        $response = $this
            ->withHeaders(['Authorization' => 'Bearer ' . $this->generalToken])
            ->patch('/api/users/' . $user->id, [
                'name' => 'New Test User',
                'email' => $user->email,
                'role' => Role::ADMIN
            ]);

        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    public function testGeneralCanNotDestroyAnotherUser()
    {
        $user = $this->getUserByEmail('another.general.user@mail.com');

        $response = $this
            ->withHeaders(['Authorization' => 'Bearer ' . $this->generalToken])
            ->delete('/api/users/' . $user->id);

        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    private function getData()
    {
        return [
            'name' => 'Test User',
            'email' => 'test.user@email.com',
            'password' => 'Wasd1234',
            'role' => Role::GENERAL
        ];
    }
}
