<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Symfony\Component\HttpFoundation\Response;
use Tests\Traits\UserTraits;

class AuthTest extends TestCase
{
    use RefreshDatabase;
    use UserTraits;

    public function setUp(): void
    {
        parent::setUp();
        $this->seed();
        $this->setUpUser();
    }

    public function testRegister()
    {
        $response = $this->post(
            '/api/register',
            $this->registerData()
        );

        $response->assertStatus(Response::HTTP_CREATED);
    }

    public function testLogin()
    {
        $this->post(
            '/api/register',
            $this->registerData()
        );

        $response = $this->post(
            '/api/login',
            [
                'email' => 'sadmin.user@mail.com',
                'password' => 'Wasd1234'
            ]
        );

        $response->assertStatus(Response::HTTP_OK);
    }

    private function registerData()
    {
        return [
            'name' => 'Test User',
            'email' => 'test.user@email.com',
            'password' => 'Wasd1234',
            'password_confirmation' => 'Wasd1234'
        ];
    }
}
