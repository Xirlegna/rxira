<?php

namespace Tests\Feature;

use App\Models\Role;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;
use Tests\Traits\UserTraits;

class UserSuperAdminTest extends TestCase
{
    use RefreshDatabase;
    use UserTraits;

    public function setUp(): void
    {
        parent::setUp();
        $this->seed();
        $this->setUpUser();
    }

    public function testSuperAdminCanListUsers()
    {
        $response = $this
            ->withHeaders(['Authorization' => 'Bearer ' . $this->sadminToken])
            ->get('/api/users');

        $response->assertStatus(Response::HTTP_OK);
        $response->assertJsonCount(2);
        $this->assertEquals(4, json_decode($response->getContent())->count);
    }

    public function testSuperAdminCanStoreUser()
    {
        $response = $this
            ->withHeaders(['Authorization' => 'Bearer ' . $this->sadminToken])
            ->post('/api/users', $this->getData());

        $clone = $this->getData();
        unset($clone["password"]);

        $response->assertStatus(Response::HTTP_CREATED);
        $response->assertJson([
            'data' => $clone
        ]);
    }

    public function testSuperAdminCanShowAnotherUser()
    {
        $user = $this->getUserByEmail('another.general.user@mail.com');

        $response = $this
            ->withHeaders(['Authorization' => 'Bearer ' . $this->sadminToken])
            ->get('/api/users/' . $user->id);

        $response->assertJson([
            'data' => [
                'name' => $user->name,
                'email' => $user->email
            ]
        ]);
    }

    public function testSuperAdminCanUpdateAnotherUser()
    {
        $user = $this->getUserByEmail('another.general.user@mail.com');

        $response = $this
            ->withHeaders(['Authorization' => 'Bearer ' . $this->sadminToken])
            ->patch('/api/users/' . $user->id, [
                'name' => 'New Test User',
                'email' => $user->email,
                'role' => Role::ADMIN
            ]);

        $response->assertStatus(Response::HTTP_OK);
        $response->assertJson([
            'data' => [
                'name' => 'New Test User',
                'email' => $user->email,
                'role' => Role::ADMIN
            ]
        ]);
    }

    public function testSuperAdminCanDestroyAnotherUser()
    {
        $user = $this->getUserByEmail('another.general.user@mail.com');

        $response = $this
            ->withHeaders(['Authorization' => 'Bearer ' . $this->sadminToken])
            ->delete('/api/users/' . $user->id);

        $this->assertCount(3, User::all());
        $response->assertStatus(Response::HTTP_NO_CONTENT);
    }

    private function getData()
    {
        return [
            'name' => 'Test User',
            'email' => 'test.user@email.com',
            'password' => 'Wasd1234',
            'role' => Role::GENERAL
        ];
    }
}
