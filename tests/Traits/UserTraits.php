<?php

namespace Tests\Traits;

use App\Models\User;

trait UserTraits
{
    private $sadminToken;
    private $adminToken;
    private $generalToken;

    public function setUpUser()
    {
        $this->sadminToken = $this->getToken('sadmin.user@mail.com');
        $this->adminToken = $this->getToken('admin.user@mail.com');
        $this->generalToken = $this->getToken('general.user@mail.com');
    }

    public function getUserByEmail(String $email): User
    {
        return User::where('email', $email)->first();
    }

    private function getToken(String $email): String
    {
        $response = $this->post(
            '/api/login',
            [
                'email' => $email,
                'password' => 'Wasd1234'
            ]
        );

        return json_decode($response->getContent())->token;
    }
}
