<?php

namespace App\Providers;

use App\Models\Role;
use App\Models\User;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * @var array
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('is-access-user', function (User $loggedInUser, User $user) {
            return $loggedInUser->id === $user->id ||
                in_array($loggedInUser->contact->role->name, [Role::SUEPR_ADMIN]);
        });

        Gate::define('is-super-admin', function (User $loggedInUser) {
            return in_array(
                $loggedInUser->contact->role->name,
                [
                    Role::SUEPR_ADMIN
                ]
            );
        });

        Gate::define('is-admin', function (User $loggedInUser) {
            return in_array(
                $loggedInUser->contact->role->name,
                [
                    Role::SUEPR_ADMIN,
                    Role::ADMIN
                ]
            );
        });

        Gate::define('is-general', function (User $loggedInUser) {
            return in_array(
                $loggedInUser->contact->role->name,
                [
                    Role::SUEPR_ADMIN,
                    Role::ADMIN,
                    Role::GENERAL
                ]
            );
        });
    }
}
