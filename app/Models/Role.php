<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    use HasFactory;

    const GENERAL = 'GENERAL';
    const ADMIN = 'ADMIN';
    const SUEPR_ADMIN = 'SUEPR_ADMIN';

    protected $fillable = ['name'];

    public function contacts()
    {
        return $this->hasMany(Contact::class, 'role_id', 'id');
    }
}
