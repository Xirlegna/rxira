<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'data' => [
                'user_id' => $this->id,
                'name' => $this->name,
                'email' => $this->email,
                'phone' => $this->contact->phone,
                'role' => $this->contact->role->name,
                'created_at' => $this->created_at->format('Y-m-d H:i:s'),
                'updated_at' => $this->updated_at->format('Y-m-d H:i:s')
            ],
            'links' => [
                'self' => $this->path()
            ]
        ];
    }
}
