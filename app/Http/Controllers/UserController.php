<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use App\Models\Role;
use App\Models\User;
use App\Rules\IsRoleRule;
use App\Http\Resources\UserResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\HttpFoundation\Response;

class UserController extends Controller
{
    public function index(Request $request)
    {
        if (!Gate::allows('is-super-admin')) {
            return response(['error' => 'Forbidden'], Response::HTTP_FORBIDDEN);
        }

        $page = $request->input('page') ? $request->input('page') : 1;
        $rowsPerPage = $request->input('rows-per-page') ? $request->input('rows-per-page') : 10;
        $order = $request->input('order') ? $request->input('order') : 'asc';
        $orderBy = $request->input('order-by') ? $request->input('order-by') : 'date';

        switch ($orderBy) {
            case 'date':
                $orderBy = 'created_at';
                break;
        }

        $offset = $page === 1 ? 0 : ($page - 1) * $rowsPerPage;

        return response([
            'list' => UserResource::collection(
                User::offset($offset)
                    ->orderBy($orderBy, $order)
                    ->limit($rowsPerPage)
                    ->get()
            ),
            'count' => count(User::all())
        ]);
    }

    public function store()
    {
        if (!Gate::allows('is-super-admin')) {
            return response(['error' => 'Forbidden'], Response::HTTP_FORBIDDEN);
        }

        $fields = $this->validateData();

        $role = Role::where('name', $fields['role'])->first();

        $user = new User;
        $user->name = $fields['name'];
        $user->email = $fields['email'];
        $user->password = bcrypt($fields['password']);
        $user->save();

        $contact = new Contact;
        $contact->role()->associate($role);

        $user->contact()->save($contact);

        $contact->save();

        return (new UserResource($user))->response()->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(User $user)
    {
        if (!Gate::allows('is-super-admin')) {
            return response(['error' => 'Forbidden'], Response::HTTP_FORBIDDEN);
        }

        return new UserResource($user);
    }

    public function update(User $user)
    {
        if (!Gate::allows('is-access-user', $user)) {
            return response(['error' => 'Forbidden'], Response::HTTP_FORBIDDEN);
        }

        $validate = request()->validate([
            'name' => ['required', 'string'],
            'email' => ['required', 'string', 'email'],
            'role' => ['required', 'string', new IsRoleRule()]
        ]);

        Contact::where('user_id', $user->id)->update([
            'role_id' => Role::where('name', $validate['role'])->first()->id
        ]);

        $user->update($validate);

        return (new UserResource($user))->response()->setStatusCode(Response::HTTP_OK);
    }

    public function destroy(User $user)
    {
        if (!Gate::allows('is-access-user', $user)) {
            return response(['error' => 'Forbidden'], Response::HTTP_FORBIDDEN);
        }

        $user->delete();

        return response([], Response::HTTP_NO_CONTENT);
    }

    public function changePassword(User $user)
    {
        if (!Gate::allows('is-access-user', $user)) {
            return response(['error' => 'Forbidden'], Response::HTTP_FORBIDDEN);
        }

        $fields = request()->validate([
            'old_password' => 'required|string',
            'password' => 'required|string|confirmed'
        ]);

        if (!Hash::check($fields['old_password'], $user->password)) {
            return response(['errors' => ['old_password' => ['Wrong password']]], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $user->update(['password' => bcrypt($fields['password'])]);

        return (new UserResource($user))->response()->setStatusCode(Response::HTTP_OK);
    }

    private function validateData()
    {
        return request()->validate([
            'name' => ['required', 'string'],
            'email' => ['required', 'string', 'email', 'unique:users,email'],
            'password' => ['required', 'string'],
            'role' => ['required', 'string', new IsRoleRule()]
        ]);
    }

    private function changePasswordValidateData()
    {
        return request()->validate([
            'old_password' => 'required|string',
            'password' => 'required|string|confirmed'
        ]);
    }
}
