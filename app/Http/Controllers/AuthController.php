<?php

namespace App\Http\Controllers;

use App\Http\Resources\LoginResource;
use App\Models\Contact;
use App\Models\Role;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\HttpFoundation\Response;

class AuthController extends Controller
{
    public function register()
    {
        $fields = $this->registerValidateData();

        $role = Role::where('name', Role::GENERAL)->first();

        $user = new User;
        $user->name = $fields['name'];
        $user->email = $fields['email'];
        $user->password = bcrypt($fields['password']);
        $user->save();

        $contact = new Contact;
        $contact->role()->associate($role);

        $user->contact()->save($contact);

        $contact->save();

        $token = $user->createToken('myapptoken')->plainTextToken;

        $response = [
            'user' => new LoginResource($user),
            'token' => $token
        ];

        return response($response, Response::HTTP_CREATED);
    }

    public function login()
    {
        $fields = $this->loginValidateData();

        $user = User::where('email', $fields['email'])->first();

        if (!$user || !Hash::check($fields['password'], $user->password)) {
            return response([
                'message' => 'Bad creds'
            ], Response::HTTP_UNAUTHORIZED);
        }

        $token = $user->createToken('myapptoken')->plainTextToken;

        $response = [
            'user' => new LoginResource($user),
            'token' => $token,
        ];

        return response($response, Response::HTTP_OK);
    }

    public function logout()
    {
        auth()->user()->tokens()->delete();

        return [
            'message' => 'Logged out'
        ];
    }

    private function registerValidateData()
    {
        return request()->validate([
            'name' => 'required|string',
            'email' => 'required|string|email|unique:users,email',
            'password' => 'required|string|confirmed'
        ]);
    }

    private function loginValidateData()
    {
        return request()->validate([
            'email' => 'required|string|email',
            'password' => 'required|string'
        ]);
    }
}
