<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Http\Resources\ProductResource;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Gate;
use Symfony\Component\HttpFoundation\Response;

class ProductController extends Controller
{
    public function index(Request $request)
    {
        if (!Gate::allows('is-general')) {
            return response(['error' => 'Forbidden'], Response::HTTP_FORBIDDEN);
        }

        $page = $request->input('page') ? $request->input('page') : 1;
        $rowsPerPage = $request->input('rows-per-page') ? $request->input('rows-per-page') : 10;
        $order = $request->input('order') ? $request->input('order') : 'asc';
        $orderBy = $request->input('order-by') ? $request->input('order-by') : 'date';

        switch ($orderBy) {
            case 'date':
                $orderBy = 'created_at';
                break;
        }

        $offset = $page === 1 ? 0 : ($page - 1) * $rowsPerPage;

        return response([
            'list' => ProductResource::collection(
                Product::offset($offset)
                    ->orderBy($orderBy, $order)
                    ->limit($rowsPerPage)
                    ->get()
            ),
            'count' => count(Product::all())
        ]);
    }

    public function store()
    {
        if (!Gate::allows('is-general')) {
            return response(['error' => 'Forbidden'], Response::HTTP_FORBIDDEN);
        }

        $fields = $this->validateData();

        $product = Product::create([
            'name' => $fields['name'],
            'slug' => Str::slug($fields['name']),
            'price' => $fields['price'],
            'description' => $fields['description']
        ]);
        
        return (new ProductResource($product))->response()->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(Product $product)
    {
        if (!Gate::allows('is-general')) {
            return response(['error' => 'Forbidden'], Response::HTTP_FORBIDDEN);
        }

        return new ProductResource($product);
    }

    public function update(Product $product)
    {
        if (!Gate::allows('is-general')) {
            return response(['error' => 'Forbidden'], Response::HTTP_FORBIDDEN);
        }

        $fields = request()->validate([
            'name' => ['required', 'string'],
            'price' => ['required', 'integer'],
            'description' => ['required', 'string']
        ]);

        $newProduct = [
            'name' => $fields['name'],
            'slug' => Str::slug($fields['name']),
            'price' => $fields['price'],
            'description' => $fields['description']
        ];

        $product->update($newProduct);

        return (new ProductResource($product))->response()->setStatusCode(Response::HTTP_OK);
    }

    public function destroy(Product $product)
    {
        if (!Gate::allows('is-general')) {
            return response(['error' => 'Forbidden'], Response::HTTP_FORBIDDEN);
        }

        $product->delete();

        return response([], Response::HTTP_NO_CONTENT);
    }

    private function validateData()
    {
        return request()->validate([
            'name' => ['required', 'string', 'unique:products'],
            'price' => ['required', 'integer'],
            'description' => ['required', 'string'],
        ]);
    }
}